---
layout: default
---

# Merge request diff ignores source branch .gitattributes file

## Summary

MR diffs do not respect the content of a source branch's .gitattributes file. For example, if a specific file is denoted as binary within a target branch's .gitattributes file, but that same line is removed in a commit on an alternate branch, a subsequent MR will not render any diffs on that specific file due to it having been marked as binary on the target branch.

## Steps to reproduce

In a testing project, create a .gitattributes file with the content: test.txt binary

- Create a test.txt file
- Create a new branch and remove the content from the .gitattributes file.
- Add a line to the test.txt file
- Create an MR and observe the diff view - the content of test.txt will not be displayed

## What is the current bug behavior?

Content within a .gitattributes file on a source branch is ignored when viewing merge request diffs.

## What is the expected correct behavior?

The source branch's .gitattributes should be used when rendering diffs.
