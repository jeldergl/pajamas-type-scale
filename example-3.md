---
layout: default
---

This example contains a mix of both scales. Based on an issue.

---

# Type scale exploration > Designer visual validation

Issue created 3 months ago by Taurie Davis

---

<div class="compact-markdown">

<h1>Context</h1>
<p>We have designed a new system for typography in GitLab. This new system changes the sizing and spacing to ensure visual hierarchy in the UI. This issue is to validate those changes before implementing them.</p>

<h1>Goals</h1>
<p>Stress test the type scale by applying it to chosen contexts.
Goals of validation is to understand: If the content hierarchy is clear. If there's a flow to the content with meaningful visual anchors. </p>

<p>If the type scale is robust enough to handle any type of content or context we'd like to apply it to. How the type scale would apply to the current UI.</p>


<h2>Validation method</h2>
<p>Recreate key parts of the UI, using the new type scales. Check for issues, and identify patterns.</p>

<h3>Pages reviewed</h3>
<p>Reviewed at 1920 and 390 viewport widths at 100% zoom.</p>

<h1>Outcomes (so far)</h1>

<h2>Decisions</h2>
<p>Following the first round of wider feedback we will do some things.</p>

</div>

---

##### Linked items

###### Blocks

Type scale exploration > Markdown author expectations

---

## Activity

Activity item

Activity item
