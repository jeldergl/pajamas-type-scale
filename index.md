---
layout: default
---

<h1>Fluid type scale</h1>

<div data-base-font-size-container>
  <h1 class="h-display" data-type="display">GitLab test for fluid type scale</h1>
  <h1 data-type="h1">GitLab test for fluid type scale</h1>
  <h2 data-type="h2">GitLab test for fluid type scale</h2>
  <h3 data-type="h3">GitLab test for fluid type scale</h3>
  <h4 data-type="h4">GitLab test for fluid type scale</h4>
  <h5 data-type="h5">GitLab test for fluid type scale</h5>
  <h6 data-type="h6">GitLab test for fluid type scale</h6>
  <p class="lg" data-type="large paragraph">GitLab Test for fluid type scale</p>
  <p data-type="paragraph">GitLab test for fluid type scale</p>
  <p class="sm" data-type="small paragraph / meta">GitLab test for fluid type scale</p>
</div>

<br /><br />
<h1>Fixed type scale</h1>

<div data-base-font-size-container class="gl-type-scale-sm">
  <h1 data-type="h1">GitLab test for fixed type scale</h1>
  <h2 data-type="h2">GitLab test for fixed type scale</h2>
  <h3 data-type="h3">GitLab test for fixed type scale</h3>
  <h4 data-type="h4">GitLab test for fixed type scale</h4>
  <h5 data-type="h5">GitLab test for fixed type scale</h5>
  <h6 data-type="h6">GitLab test for fixed type scale</h6>
  <p class="lg" data-type="large paragraph">GitLab Test for fixed type scale</p>
  <p data-type="paragraph">GitLab test for fixed type scale</p>
  <p class="sm" data-type="small paragraph / meta">GitLab test for fixed type scale</p>
</div>
