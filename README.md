# Type scale test

Type scale test in an isolated project.

## Set up

To run locally:

- Run `bundle install`
- Run `bundle exec jekyll serve` and visit http://127.0.0.1:4000/pajamas-type-scale/
- For live watch and reload run `bundle exec jekyll serve -w -l`

View the demo site at https://jeldergl.gitlab.io/pajamas-type-scale/
