---
layout: default
---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

<hr>

# Heading 1

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

## Heading 2

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

### Heading 3

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

#### Heading 4

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

##### Heading 5

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

###### Heading 6

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.
